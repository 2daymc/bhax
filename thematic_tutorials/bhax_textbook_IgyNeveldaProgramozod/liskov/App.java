class Parent
{
    public void saySomething()
    {
        System.out.println("Parent says: bla bla bla");
    }
}
class Child extends Parent
{
    public void echoSomething(String msg)
    {
        System.out.println(msg);
    }
}
public class App
{
    public static void main(String[] args)
    {
        Parent p = new Parent();
        Parent p2 = new Child();
        
        System.out.println("szülő metódusának meghívása");
        p.saySomething();
        
        System.out.println("gyerek metódusának meghívása referencián keresztül a szülő által");
        p2.echoSomething("nem fog működni");
        
    }
}

