#include <iostream>
#include <string>

class Parent
{
public:
        void saySomething()
    {
        std::cout << "Parent says: bla bla bla\n";
    }
};
class Child : public Parent
{
public:
        void echoSomething(std::string msg)
    {
       std::cout << msg << "\n";
    }
};



class App
{
   int main()
    {
        Parent* p = new Parent();
        Parent* p2 = new Child();
        
        std::cout << "Invoking method of parent\n";
        p->saySomething();
        
        std::cout << "gyerek metódusának meghívása referencián keresztül a szülő által\n";
        p2->echoSomething("nem fog működni");
        
        delete p;
        delete p2;
        
    }
};

