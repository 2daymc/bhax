  g.drawLine(
                    (int)((reZ - a)/dx),
                    (int)((d - imZ)/dy),
                    (int)((ujreZ - a)/dx),
                    (int)((d - ujimZ)/dy)
                    );

 // Egér kattintó események feldolgozása:
        addMouseListener(new java.awt.event.MouseAdapter() {
            // Egér kattintással jelöljük ki a nagyítandó területet
            // bal felső sarkát vagy ugyancsak egér kattintással
            // vizsgáljuk egy adott pont iterációit:
            public void mousePressed(java.awt.event.MouseEvent m) {
                // Az egérmutató pozíciója
                x = m.getX();
                y = m.getY();
                // Az 1. egér gombbal a nagyítandó terület kijelölését
                // végezzük:
                if(m.getButton() == java.awt.event.MouseEvent.BUTTON1 ) {
                    // A nagyítandó kijelölt területet bal felső sarka: (x,y)
                    // és szélessége (majd a vonszolás növeli)
                    mx = 0;
                    my = 0;
                    repaint();
                } else {
                    // Nem az 1. egér gombbal az egérmutató mutatta c
                    // komplex számból indított iterációkat vizsgálhatjuk
                    MandelbrotIterációk iterációk =
                            new MandelbrotIterációk(
                            MandelbrotHalmazNagyító.this, 50);
                    new Thread(iterációk).start();
                }
            }           

public class MandelbrotIterációk implements Runnable{

/** Az vizsgált pontból induló iterációk bemutatása. */
    public void run() {
        /* Az alábbi kód javarészt a MandelbrotHalmaz.java számolást 
         végző run() módszeréből származik, hiszen ugyanazt csináljuk,
         csak most nem a hálón megyünk végig, hanem a háló adott a
         példányosításunkkor az egérmutató mutatta csomópontjában (ennek
         felel meg a c kompelx szám) számolunk, tehát a két külső for 
         ciklus nem kell. */
        // A [a,b]x[c,d] tartományon milyen sűrű a
        // megadott szélesség, magasság háló:
        double dx = (b-a)/szélesség;
        double dy = (d-c)/magasság;
        double reC, imC, reZ, imZ, ujreZ, ujimZ;
        // Hány iterációt csináltunk?
        int iteráció = 0;
        // c = (reC, imC) a háló rácspontjainak
        // megfelelő komplex szám
        reC = a+k*dx;
        imC = d-j*dy;
        // z_0 = 0 = (reZ, imZ)
        reZ = 0;
        imZ = 0;
        iteráció = 0;
        // z_{n+1} = z_n * z_n + c iterációk
        // számítása, amíg |z_n| < 2 vagy még
        // nem értük el a 255 iterációt, ha
        // viszont elértük, akkor úgy vesszük,
        // hogy a kiinduláci c komplex számra
        // az iteráció konvergens, azaz a c a
        // Mandelbrot halmaz eleme
        while(reZ*reZ + imZ*imZ < 4 && iteráció < 255) {
            // z_{n+1} = z_n * z_n + c
            ujreZ = reZ*reZ - imZ*imZ + reC;
            ujimZ = 2*reZ*imZ + imC;
         
            // az iteráció (reZ, imZ) -> (ujreZ, ujimZ)
            // ezt az egyenest kell kirajzolnunk, de most
            // a komplex számokat vissza kell transzformálnunk
            // a rács oszlop, sor koordinátájává:
            java.awt.Graphics g = kép.getGraphics();
            g.setColor(java.awt.Color.WHITE);
            g.drawLine(
                    (int)((reZ - a)/dx),
                    (int)((d - imZ)/dy),
                    (int)((ujreZ - a)/dx),
                    (int)((d - ujimZ)/dy)
                    );
            g.dispose();
            nagyító.repaint();
            
            reZ = ujreZ;
            imZ = ujimZ;
            
            ++iteráció;
            // Várakozunk, hogy közben csodálhassuk az iteráció
            // látványát:
            try {
                Thread.sleep(várakozás);
            } catch (InterruptedException e) {}
        }
    }       
