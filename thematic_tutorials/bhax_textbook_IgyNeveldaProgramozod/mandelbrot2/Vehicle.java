public class Vehicle {

	public boolean moving() {
		// TODO - implement Vehicle.moving
		throw new UnsupportedOperationException();
	}


	public class Car {

		public boolean moving() {
			// TODO - implement Car.moving
			throw new UnsupportedOperationException();
		}

		public boolean makingWeirdNoises() {
			// TODO - implement Car.makingWeirdNoises
			throw new UnsupportedOperationException();
		}

	}


	public class Plane {

		public boolean flying() {
			// TODO - implement Plane.flying
			throw new UnsupportedOperationException();
		}

		public boolean moving() {
			// TODO - implement Plane.moving
			throw new UnsupportedOperationException();
		}

	}

}