package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LZWBinFaTest {

	LZWBinFa binfa = new LZWBinFa();
	
	@Test
	void test() {
		String input = "101011101010000011110110";
		for(int i=0;i<input.length();i++) {
			binfa.add(input.charAt(i));
		}
			
		assertEquals(4, binfa.getMelyseg());
	}

}

